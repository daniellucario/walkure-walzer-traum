import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router"
import { LoginService } from '../../services/login.service';
import { ToastService } from '../../services/toast.service'
import { User } from '../../models/user.model';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss'],
  providers: [LoginService]
})
export class WelcomeComponent implements OnInit {

  public user: User;

  constructor(
    private loginService: LoginService,
    private router: Router,
    protected toastService: ToastService,
    private cookieService: CookieService) {
    this.user = new User();
  }

  ngOnInit(): void {
  }

  validateLogin() {
    if (this.user.username && this.user.password) {
      this.loginService.validateLogin(this.user).subscribe(result => {
        //console.log('result is ', result);
        if (result['status'] === 'success') {
          this.toastService.success('Acceso autorizado', 'done')
          const _id = result['data'][0]._id
          this.cookieService.set('user_id', _id);
          this.router.navigate(['/inicio']);
        } else {
          this.toastService.error('Acceso denegado', 'warning_amber')
        }

      }, error => {
        console.log('error is ', error);
      });
    } else {
      this.toastService.error('Los campos usuario y contraseña son requeridos', 'warning_amber')
    }
  }
}
