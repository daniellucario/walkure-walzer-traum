import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../services/product.service'
import { ToastService } from '../../../services/toast.service'

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  constructor(private productService: ProductService, private toast: ToastService) { }

  products: any;
  modal_create: any

  ngOnInit(): void {
    this.load_products()
  }

  load_products() {
    this.productService.getAll().subscribe(result => {
      this.products = result
    }, error => {
      console.log('error is ', error);
      this.toast.error('Algo salió mal al leer la base de datos', 'report_problem')
    });
  }

  delete(id) {
    if (confirm('Desea eliminar este registro?')) {
      this.productService.delete(id).subscribe(result => {
        this.toast.success('Eliminado exitosamente', 'done')
        this.load_products()
      }, error => {
        console.log('error is ', error);
        this.toast.error('Algo salió mal', 'report_problem')
      })
    }
  }

  submit_form_agregar(form, modal) {
    let product = form.value
    if (product.name && product.stock && product.barcode && product.price) {
      this.productService.post(product).subscribe(result => {
        this.toast.success('Agregado exitosamente', 'done')
        form.reset()
        this.load_products()
        modal.hide()
      }, error => {
        console.log('error is ', error);
        this.toast.error('Algo salió mal al escribir en la base de datos', 'report_problem')
      })
    } else {
      this.toast.warning('Los datos marcados con * son obligatorios', 'report_problem')
    }
  }
}
