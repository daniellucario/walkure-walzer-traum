import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit {
  constructor() { }
  product: boolean = true
  category: boolean = false
  provider: boolean = false
  ngOnInit(): void {
  }
}
