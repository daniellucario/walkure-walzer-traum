import { Component, Inject, SecurityContext, OnInit } from '@angular/core';
import { DomSanitizer, SafeValue } from '@angular/platform-browser';
import { EncrDecrService } from '../../services/encr-decr.service';

@Component({
  selector: 'app-security',
  templateUrl: './security.component.html',
  styleUrls: ['./security.component.scss']
})
export class SecurityComponent implements OnInit {

  proteccion: boolean = true;
  validacion: boolean = false;
  encriptacion: boolean = false;
  imp: string;
  out: string;

  constructor(@Inject(DomSanitizer) private readonly sanitizer: DomSanitizer, private EncrDecr: EncrDecrService) { }

  ngOnInit(): void {
  }

  //sanitizer
  unwrap(value: SafeValue | null): string {
    return this.sanitizer.sanitize(SecurityContext.HTML, value) || '';
  }

  enc(fe) {
    this.imp = this.EncrDecr.set('123456$#@$^@1ERF', fe.value.inputText)
  }
  dec(fd) {
    this.out = this.EncrDecr.get('123456$#@$^@1ERF', fd.value.inputText)
  }
}
