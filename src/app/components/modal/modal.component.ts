import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ww-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  constructor() { }

  visible: boolean = false

  ngOnInit(): void {
  }

  show() {
    this.visible = true
  }

  hide() {
    this.visible = false
  }
}
