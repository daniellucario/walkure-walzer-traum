import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Subscription } from 'rxjs';

import { Toast } from '../../models/toast.model';
import { ToastService } from '../../services/toast.service';
@Component({
  selector: 'ww-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss']
})
export class ToastComponent implements OnInit, OnDestroy {

  @Input() id = 'default-toast';
  @Input() fade = true;

  toasts: Toast[] = [];

  toastSubscription: Subscription;
  routeSubscription: Subscription;

  constructor(private toastService: ToastService, private router: Router) { }

  ngOnInit(): void {
    // subscribe to new toast notifications
    this.toastSubscription = this.toastService.onToast(this.id)
      .subscribe(toast => {
        // clear toasts when an empty toast is received
        if (!toast.message) {
          // filter out toasts without 'keepAfterRouteChange' flag
          this.toasts = this.toasts.filter(x => x.keepAfterRouteChange);

          // remove 'keepAfterRouteChange' flag on the rest
          this.toasts.forEach(x => delete x.keepAfterRouteChange);
          return;
        }

        // add toast to array
        this.toasts.push(toast);

        // auto close toast if required
        setTimeout(() => this.removetoast(toast), 4000);
      });
  }

  ngOnDestroy() {
    // unsubscribe to avoid memory leaks
    this.toastSubscription.unsubscribe();
    this.routeSubscription.unsubscribe();
  }

  removetoast(toast: Toast) {
    // check if already removed to prevent error on auto close
    if (!this.toasts.includes(toast)) return;

    this.toasts = this.toasts.filter(x => x !== toast);
  }

}
