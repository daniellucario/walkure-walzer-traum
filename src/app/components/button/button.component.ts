import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ww-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() color: string = 'default';
  constructor() { }

  ngOnInit(): void {
  }

}
