import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ww-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.scss']
})
export class TextComponent implements OnInit {
  @Input() placeholder: string = 'Entrada de texto';
  constructor() { }

  ngOnInit(): void {
  }

}
