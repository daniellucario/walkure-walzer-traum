import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';

import { Toast } from '../models/toast.model';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  private subject = new Subject<Toast>();
  private defaultId = 'default-toast';

  constructor() { }

  // enable subscribing to toasts observable
  onToast(id = this.defaultId): Observable<Toast> {
    return this.subject.asObservable().pipe(filter(x => x && x.id === id));
  }

  // convenience methods
  success(message: string, icon: string) {
    this.toast(new Toast({ color: 'leaf', message, icon }));
  }

  error(message: string, icon: string) {
    this.toast(new Toast({ color: 'sunset', message, icon }));
  }

  warning(message: string, icon: string) {
    this.toast(new Toast({ color: 'rose', message, icon }));
  }

  info(message: string, icon: string) {
    this.toast(new Toast({ color: 'sea', message, icon }));
  }

  // main toast method    
  toast(toast: Toast) {
    toast.id = toast.id || this.defaultId;
    this.subject.next(toast);
  }

  // clear toasts
  clear(id = this.defaultId) {
    this.subject.next(new Toast({ id }));
  }
}
