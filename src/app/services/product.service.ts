import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from '../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get('/api/product/get', {})
  }

  post(product) {
    return this.http.post('/api/product/post', product)
  }

  getOne(id) {
    return this.http.get('/api/product/get/' + id, {})
  }

  delete(id) {
    return this.http.delete('/api/product/' + id, {})
  }

  patch(id) {
    return this.http.patch('/api/product/' + id, {})
  }
}
