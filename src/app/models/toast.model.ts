export class Toast {
    id: string;
    message: string;
    icon: String;
    color: String;
    keepAfterRouteChange: unknown;

    constructor(init?: Partial<Toast>) {
        Object.assign(this, init);
    }
}