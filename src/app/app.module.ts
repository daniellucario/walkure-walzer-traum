import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { MatTooltipModule } from '@angular/material/tooltip';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';

import { WelcomeComponent } from './pages/welcome/welcome.component';
import { NotfoundComponent } from './pages/notfound/notfound.component';
import { TextComponent } from './components/text/text.component';
import { ButtonComponent } from './components/button/button.component';
import { LinkComponent } from './components/link/link.component';
import { ComboComponent } from './components/combo/combo.component';
import { RadioComponent } from './components/radio/radio.component';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { MenuComponent } from './pages/menu/menu.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HomeComponent } from './components/home/home.component';
import { ExternalComponent } from './components/external/external.component';
import { EmployesComponent } from './pages/employes/employes.component';
import { InventoryComponent } from './pages/inventory/inventory.component';
import { TeamsComponent } from './pages/teams/teams.component';
import { SalesComponent } from './pages/sales/sales.component';
import { ReportsComponent } from './pages/reports/reports.component';
import { BillsComponent } from './pages/bills/bills.component';
import { SecurityComponent } from './pages/security/security.component';
import { ClickStopPropagationDirective } from './directives/click-stop-propagation.directive';
import { NgDompurifyModule } from '@tinkoff/ng-dompurify';
import { EncrDecrService } from './services/encr-decr.service';
import { ContainerComponent } from './components/container/container.component';
import { ModalComponent } from './components/modal/modal.component';
import { ProfileComponent } from './components/profile/profile.component';
import { TableComponent } from './components/table/table.component';
import { ModalFormComponent } from './components/modal-form/modal-form.component';
import { ToastComponent } from './components/toast/toast.component';
import { ProductComponent } from './pages/inventory/product/product.component';
import { CategoryComponent } from './pages/inventory/category/category.component';
import { ProviderComponent } from './pages/inventory/provider/provider.component';
import { CookieService } from 'ngx-cookie-service';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    NotfoundComponent,
    TextComponent,
    ButtonComponent,
    LinkComponent,
    ComboComponent,
    RadioComponent,
    CheckboxComponent,
    MenuComponent,
    SidebarComponent,
    HomeComponent,
    ExternalComponent,
    EmployesComponent,
    InventoryComponent,
    TeamsComponent,
    SalesComponent,
    ReportsComponent,
    BillsComponent,
    SecurityComponent,
    ClickStopPropagationDirective,
    ContainerComponent,
    ModalComponent,
    ProfileComponent,
    TableComponent,
    ModalFormComponent,
    ToastComponent,
    ProductComponent,
    CategoryComponent,
    ProviderComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MatTooltipModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    NgDompurifyModule
  ],
  providers: [
    EncrDecrService,
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
