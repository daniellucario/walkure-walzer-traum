import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './pages/welcome/welcome.component';
import { NotfoundComponent } from './pages/notfound/notfound.component';
import { MenuComponent } from './pages/menu/menu.component';
import { SecurityComponent } from './pages/security/security.component';
import { InventoryComponent } from './pages/inventory/inventory.component';

const routes: Routes = [
  { path: '', component: WelcomeComponent },
  { path: 'inicio', component: MenuComponent },
  { path: 'empleados', component: MenuComponent },
  { path: 'seguridad', component: SecurityComponent },
  { path: 'inventario', component: InventoryComponent },
  { path: '**', component: NotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
