// schema of the product table
const User = require('../model/user')


//Get all
exports.get_all_user = async (req, res) => {
    try {
        // finding all the product in the table
        const user = await User.find()
        // To check in the console 
        console.log(user)
        res.send(user)
    } catch (err) {
        res.send({ message: err })
    }
}

//Login
exports.login_user = async (req, res) => {
    try {
        User.find({
            username: req.body.username, password: req.body.password
        }, function (err, user) {
            if (err) throw err;
            if (user.length === 1) {
                return res.status(200).json({
                    status: 'success',
                    data: user
                })
            } else {
                return res.status(200).json({
                    status: 'fail',
                    message: 'Login Failed'
                })
            }

        })
    } catch (err) {
        res.send({ message: err })
    }
}

//Create user
exports.create_user = async (req, res) => {
    console.log(req.body)

    // retrieving data from json body of post request
    const user = new User({
        username: req.body.username,
        password: req.body.password,
        name: req.body.name,
        last_name: req.body.last_name,
        association: req.body.association,
        status: 'active',
        type: 'normal'
    });
    user.save()
        .then(result => {
            res.status(200).send({
                _id: result._id
            })
        })
        .catch(err => {
            res.send({ message: err })
        })
}
