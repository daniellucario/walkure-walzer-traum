// schema of the product table
const Product = require('../model/products')

//Get all
exports.get_all_product = async (req, res) => {
    try {
        // finding all the product in the table
        const products = await Product.find()
        // To check in the console 
        console.log(products)
        res.send(products)
    } catch (err) {
        res.send({ message: err })
    }
}

//get one
exports.get_single_product = (req, res) => {
    const id = req.params.productId; //get the productid from params
    Product.findById(id)
        .then(result => {
            res.send(result) // if product is found then returned
        })
        .catch(err => {
            res.send({ message: err }) //gives error if product not found
        })
}

//create
exports.create_product = async (req, res) => {

    // retrieving data from json body of post request
    const product = new Product({
        name: req.body.name,
        type: req.body.type,
        weight: req.body.weight,
        size: req.body.size,
        stock: req.body.stock,
        description: req.body.description,
        details: req.body.details,
        codebar: req.body.codebar,
        price: req.body.price,
        providers: req.body.providers,
        category: req.body.category
    });
    product.save()
        .then(result => {
            res.status(200).send({
                _id: result._id,
            })
        })
        .catch(err => {
            res.send({ message: err })
        })
}

//update
exports.patch_product_details = (req, res) => {
    const id = req.params.productId; // get product ID from params
    const update = {}

    Product.updateOne({ _id: id }, req.body)
        .exec()
        .then(result => {
            res.send(result)
        })
        .catch(err => {
            res.send(err)
        })
}

//delete
exports.delete_product = (req, res) => {
    const id = req.params.productId; //checks for productId to delete
    Product.deleteOne({ _id: id }) // removes product from table
        .exec()
        .then(result => {
            res.send(result)  //sends the result back
        })
        .catch(err => {
            res.send(err)    // sends error if product is not updated
        })
}