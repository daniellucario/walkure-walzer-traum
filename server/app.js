const express = require('express')
const app = express()
const cors = require('cors')
const mongoose = require('mongoose')
require('dotenv/config')

const CONNECTION = "mongodb+srv://daleth:cbDHWKCnE3hRMyjD@cluster0.83hnu.mongodb.net/walkure-waltz?retryWrites=true&w=majority"

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.listen(3000, () => console.log('server running, port 3000'))

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, PATCH, DELETE')
        return res.status(200).json({})
    }
    next();
});

mongoose.connect(
    CONNECTION,
    { useNewUrlParser: true, useUnifiedTopology: true },
    (result) => {
        console.log(result)
    })
mongoose.Promise = global.Promise

const ProductRoute = require('./routes/product');
app.use('/api/product', ProductRoute)

const UserRoute = require('./routes/user');
app.use('/api/user', UserRoute)


/*
app.post('/api/user/login', (req, res) => {
    mongoose.connect(url, { useMongoClient: true }, function (err) {
        if (err) throw err;
        User.find({
            username: req.body.username, password: req.body.password
        }, function (err, user) {
            if (err) throw err;
            if (user.length === 1) {
                return res.status(200).json({
                    status: 'success',
                    data: user
                })
            } else {
                return res.status(200).json({
                    status: 'fail',
                    message: 'Login Failed'
                })
            }

        })
    });
})
*/