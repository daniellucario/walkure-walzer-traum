const express = require('express')
const router = express.Router()

const ProductController = require('../controller/product')

router.get('/get', ProductController.get_all_product);
router.post('/post', ProductController.create_product)
router.get('/:productId', ProductController.get_single_product)
router.patch('/:productId', ProductController.patch_product_details)
router.delete('/:productId', ProductController.delete_product)

module.exports = router