const express = require('express')
const router = express.Router()

const UserController = require('../controller/user')

router.get('/get', UserController.get_all_user)
router.post('/login', UserController.login_user)
router.post('/post', UserController.create_user)
//router.get('/:userId', UserController.get_single_user)
//router.patch('/:userId', UserController.patch_user_details)
//router.delete('/:userId', UserController.delete_user)

module.exports = router