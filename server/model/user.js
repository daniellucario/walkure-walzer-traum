const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create a schema
const UsersSchema = new Schema({
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    name: { type: String, required: true },
    last_name: { type: String, required: true },
    association: { type: String, required: true },
    status: { type: String, required: true },
    type: { type: String, required: true }
}, { collection: 'users' });

const User = mongoose.model('users', UsersSchema);

module.exports = User;