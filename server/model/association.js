const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create a schema
const associationsSchema = new Schema({
    name: { type: String, required: true },
    members: { type: Array, required: true },
    services: { type: Array, required: true },
    expiration: { type: Date, required: true }
}, { collection: 'associations' });

const associations = mongoose.model('associations', associationsSchema);

module.exports = associations;