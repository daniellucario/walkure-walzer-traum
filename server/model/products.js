const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create a schema
const productsSchema = new Schema({
    name: { type: String, required: true },
    type: { type: String },
    weight: { type: String },
    size: { type: String },
    stock: { type: Number, required: true },
    description: { type: String },
    details: { type: String },
    codebar: { type: String, required: false },
    price: { type: Number, required: true },
    providers: { type: Array },
    catergory: { type: Array }
}, { collection: 'products' });

const products = mongoose.model('products', productsSchema);
module.exports = products;